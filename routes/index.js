const express = require('express');
const router = express.Router();
const List = require('../list');
const mongoose = require('mongoose');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {title: 'Express'});
});

router.post('/addlist', function(req, res) {
    const newList = new List({
        licence: req.body.licence,
        o1: req.body.o1,
        c1: req.body.c1,
        o2: req.body.o2,
        c2: req.body.c2,
        o3: req.body.o3,
        c3: req.body.c3,
        o4: req.body.o4,
        c4: req.body.c4,
        o5: req.body.o5,
        c5: req.body.c5,
        o6: req.body.o6,
        c6: req.body.c6,
        o7: req.body.o7,
        c7: req.body.c7,
        o8: req.body.o8,
        c8: req.body.c8,
        o9: req.body.o9,
        c9: req.body.c9,
        o10: req.body.o10,
        c10: req.body.c10,
        o11: req.body.o11,
        c11: req.body.c11,
        o12: req.body.o12,
        c12: req.body.c12,
        o13: req.body.o13,
        c13: req.body.c13,
        o14: req.body.o14,
        c14: req.body.c14,
        o15: req.body.o15,
        c15: req.body.c15,
        o16: req.body.o16,
        c16: req.body.c16,
        o17: req.body.o17,
        c17: req.body.c17,
        o18: req.body.o18,
        c18: req.body.c18,
        o19: req.body.o19,
        c19: req.body.c19,
        o20: req.body.o20,
        c20: req.body.c20,

    });
    newList.save(function(err) {
        if(err) {
            return handleError(err);
        } else {
            console.log('form has been saved');
        }
    });
    res.redirect('/');
});


router.get('/findlist/:licence', (req, res) => {
    const licence = req.params.licence;
    List.find({'licence': licence}, function(err, list) {
        if (err) return handleError(err);
        res.send(list[0]);
    });
});

router.post('/updatelist/:licence', (req, res) => {
    console.log('id to be updated: ' + req.params.licence);
    const query = {'licence': req.params.licence};
    const update = req.body;
    const options = {new: true};
    List.findOneAndUpdate(query, update, options, function(err, list) {
        if (err) {
            console.log('got an error');
        }
        console.log(list);
        res.redirect('/');
    });
});

module.exports = router;
