# README #


### Repo for SSSF course project ###

SSSF Project idea - Checklist for buying a car

Website where you open a template of a checklist that you use for checking faults in a car that you are buying. That template has all the items you need to check in a car. Each item has a checkbox for “ok” and “faulty”, and a comment field. Website is mobile browser friendly. Checklist can be saved and it’s given a 6-digit id that can be used for opening the checklist later, no account creation required. Checklist can be bookmarked in the browser.

“I want to buy a car that has no faults”
“I want on my phone a list of items to check on a car that I’m buying and easily make notes and mark items as checked.”