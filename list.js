/**
 * Created by morttiaittokoski on 26/04/2017.
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const listSchema = new Schema({
    licence: String,
    o1: String,
    c1: String,
    o2: String,
    c2: String,
    o3: String,
    c3: String,
    o4: String,
    c4: String,
    o5: String,
    c5: String,
    o6: String,
    c6: String,
    o7: String,
    c7: String,
    o8: String,
    c8: String,
    o9: String,
    c9: String,
    o10: String,
    c10: String,
    o11: String,
    c11: String,
    o12: String,
    c12: String,
    o13: String,
    c13: String,
    o14: String,
    c14: String,
    o15: String,
    c15: String,
    o16: String,
    c16: String,
    o17: String,
    c17: String,
    o18: String,
    c18: String,
    o19: String,
    c19: String,
    o20: String,
    c20: String,
});

const List = mongoose.model('List', listSchema);

module.exports = List;


